package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import sheridan.Celsius;

public class CelsiusTest {

	@Test
	public void testIsValidFahrenheitToCelsiusConversionRegular() {
		int isValidConversion = Celsius.fromFahrenheit(23);
		System.out.println("1 " + isValidConversion);
		assertTrue("Invalid to Celsius Conversion!", isValidConversion == -5);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidFahrenheitToCelsiusConversionException() {
		int isValidConversion = Celsius.fromFahrenheit(-32);
		System.out.println("2 " + isValidConversion);
		assertFalse("Invalid to Celsius Conversion!", isValidConversion == Integer.parseInt("0A"));
	}
	
	@Test
	public void testIsValidFahrenheitToCelsiusConversionBoundaryIn() {
		int isValidConversion = Celsius.fromFahrenheit(0);
		System.out.println("3 " + isValidConversion);
		assertTrue("Invalid to Celsius Conversion!", isValidConversion == (int)(-17.7));
	}
	
	@Test(expected=AssertionError.class)
	public void testIsValidFahrenheitToCelsiusConversionBoundaryOut() {
		int isValidConversion = Celsius.fromFahrenheit(6);
		System.out.println("4 " + isValidConversion);
		assertFalse("Invalid to Celsius Conversion!", isValidConversion == (int)(-14.4));
	}
	
}
